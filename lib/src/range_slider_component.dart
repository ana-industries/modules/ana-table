import 'dart:async' show StreamController;

import 'package:angular/angular.dart';
import 'package:angular_components/laminate/popup/popup.dart';
import 'package:angular_components/material_button/material_button.dart';
import 'package:angular_components/material_popup/material_popup.dart';
import 'package:angular_components/material_slider/material_slider.dart';
import 'package:tuple/tuple.dart' show Tuple2;

/// RangeSliderComponent is a two way range slider for any number value.
///
/// It is not perfect. Need lot of UI works.
///
@Component(
  selector: 'range-slider',
  directives: [
    MaterialButtonComponent,
    MaterialPopupComponent,
    MaterialSliderComponent,
    PopupSourceDirective
  ],
  styleUrls: ['range_slider_component.css'],
  templateUrl: 'range_slider_component.html',
)
class RangeSliderComponent implements OnInit {
  @Input()
  Tuple2<num, num> value;
  @Input()
  Tuple2<num, num> limit;
  @Input()
  bool isSingleMode = false;

  final _valueChange = StreamController<Tuple2<num, num>>();
  @Output()
  Stream<Tuple2<num, num>> get valueChange => _valueChange.stream;

  bool visible = false;

  @override
  void ngOnInit() {
    if (value == null) {
      value = limit;
    }
  }

  void lowerSliderChange(num v) {
    value = value.withItem1(v);
    _valueChange.add(value);
  }

  void upperSliderChange(num v) {
    value = value.withItem2(v);
    _valueChange.add(value);
  }
}
