import 'dart:async' show StreamController;

import 'package:angular/angular.dart';
import 'package:angular_forms/angular_forms.dart';
import 'package:angular_components/laminate/enums/alignment.dart';
import 'package:angular_components/laminate/popup/popup.dart';
import 'package:angular_components/material_button/material_button.dart';
import 'package:angular_components/material_popup/material_popup.dart';
import 'package:angular_components/material_select/material_dropdown_select.dart';
import 'package:angular_components/material_select/material_dropdown_select_accessor.dart';
import 'package:angular_components/material_toggle/material_toggle.dart';

import 'package:ana_metadata/filter.dart';

import 'range_slider_component.dart';
import 'range_slider_accessor.dart';

/// FilterComponent display a data-driven filter option menu.
///
/// Each filter type (drop down, toggle or number) is determine by each `entries`.
///
@Component(
    selector: 'filter',
    directives: [
      DropdownSelectValueAccessor,
      MaterialButtonComponent,
      MaterialDropdownSelectComponent,
      MaterialPopupComponent,
      MaterialToggleComponent,
      PopupSourceDirective,
      NgControlName,
      NgIf,
      NgFor,
      NgFormModel,
      NgSwitch,
      NgSwitchWhen,
      RangeSliderComponent,
      RangeSliderValueAccessor,
    ],
    exports: [FilterType, RelativePosition],
    styleUrls: ['filter_component.css'],
    templateUrl: 'filter_component.html')
class FilterComponent implements AfterChanges {
  @Input()
  List<FilterMetaData> entries;
  @Input()
  List<Filter> presets;

  final _filterChange = StreamController<List<Filter>>();
  @Output()
  Stream get filterChange => _filterChange.stream;

  ControlGroup forming;
  bool visible = false;

  @override
  void ngAfterChanges() {
    if (entries == null) {
      return;
    }
    var entryMap = Map<String, AbstractControl>();
    presets.forEach((preset) {
      entryMap[preset.id] = Control(preset.value);
    });
    forming = ControlGroup(entryMap);
  }

  void apply() {
    visible = false;

    _filterChange.add(presets
        .map((preset) =>
            Filter(preset.id, preset.type, forming.controls[preset.id].value))
        .toList());
  }

  String buttonTextFor(String entryId) {
    final value = forming.controls[entryId].value;
    return value?.toString() ?? 'All';
  }
}
