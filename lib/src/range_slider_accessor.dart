import 'package:angular/angular.dart';
import 'package:angular_forms/angular_forms.dart';
import 'package:tuple/tuple.dart' show Tuple2;

import 'range_slider_component.dart';

@Directive(
  selector: 'range-slider[ngControl],range-slider[ngFormControl]',
  providers: [
    ExistingProvider.forToken(ngValueAccessor, RangeSliderValueAccessor),
  ],
)
class RangeSliderValueAccessor implements ControlValueAccessor {
  final RangeSliderComponent _rs;

  RangeSliderValueAccessor(this._rs);

  @override
  void writeValue(value) {
    _rs.value = value;
  }

  @override
  void onDisabledChanged(bool isDisabled) {}

  @override
  void registerOnChange(ChangeFunction<Tuple2<num, num>> fn) {
    _rs.valueChange.listen(fn);
  }

  @override
  void registerOnTouched(TouchFunction fn) {}
}
