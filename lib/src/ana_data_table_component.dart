import 'dart:async' show StreamController;

import 'package:angular/angular.dart';
import 'package:angular_forms/angular_forms.dart' show NgModel;
import 'package:angular_components/button_decorator/button_decorator.dart';
import 'package:angular_components/material_button/material_button.dart';
import 'package:angular_components/material_chips/material_chip.dart';
import 'package:angular_components/material_chips/material_chips.dart';
import 'package:angular_components/material_icon/material_icon.dart';
import 'package:angular_components/material_select/material_dropdown_select.dart';
import 'package:angular_components/material_select/material_dropdown_select_accessor.dart';
import 'package:angular_components/model/selection/selection_model.dart';
import 'package:tuple/tuple.dart' show Tuple2, Tuple3;

import 'package:ana_metadata/filter.dart';
import 'package:ana_metadata/metadata.dart';

import 'ana_table_component.dart';
import 'filter_component.dart';

/// AnaDataTableComponent add more functionality on top of ANATableComponent.
/// With Pagination, Filter and Column Selection. It is tuned to use ANA_metadata derived data.
///
/// This component do not manipulate the data in anyway. It display whatever passed to it, and
/// propagrate all the user action back to the user of this class.
///
/// For example: `filterChange` is fire when Filter setting is changed. This component will not
/// manipulate the input data with that information. It is up to the user of this class to handle the event,
/// and pass in a new set of data.
///
/// When `schema` is present, it will help this class determine the important columns, and other auxillary metadata for input
/// `filteredData` is the rows of data to display into the table. They are in `Propsy`, so that a change of column will chose the
/// correct data to display.
/// `filterPresets` is the applied fitler option. Usually this is empty, meaning, all filter is off.
/// `filterEntries` are all the fitre option for this current table.
///
@Component(
    selector: 'ana-data-table',
    directives: [
      ButtonDirective,
      DropdownSelectValueAccessor,
      MaterialButtonComponent,
      MaterialChipComponent,
      MaterialChipsComponent,
      MaterialIconComponent,
      MaterialDropdownSelectComponent,
      MultiDropdownSelectValueAccessor,
      NgIf,
      NgModel,
      AnaTableComponent,
      FilterComponent,
    ],
    styleUrls: ['ana_data_table_component.css'],
    templateUrl: 'ana_data_table_component.html')
class AnaDataTableComponent implements OnInit, OnChanges {
  @Input()
  Schema schema;
  @Input()
  List<Propsy> filteredData;
  @Input()
  List<Filter> filterPresets;
  @Input()
  List<FilterMetaData> filterEntries;
  @Input()
  SelectionModel<String> selectionModel;
  @Input()
  List<String> quickEditFields;
  @Input()
  bool hasNextPage;
  @Input()
  bool hasPreviousPage;
  @Input()
  int pageLimit;
  @Input()
  int page = 0;
  @Input()
  String title;

  final _selection = StreamController<List<String>>();
  final _filterChange = StreamController<List<Filter>>();
  final _nextPageTrigger = StreamController<void>();
  final _previousPageTrigger = StreamController<void>();
  final _pageLimitChange = StreamController<int>();
  final _quickEdit = StreamController<Tuple3<String, String, String>>();

  @Output()
  Stream get selectionChange => _selection.stream;
  @Output('filterChange')
  Stream get interalFilterChange => _filterChange.stream;
  @Output('nextPage')
  Stream get nextPageTrigger => _nextPageTrigger.stream;
  @Output('previousPage')
  Stream get previousPageTrigger => _previousPageTrigger.stream;
  @Output('pageLimitChange')
  Stream get internalPageLimitChange => _pageLimitChange.stream;
  @Output()
  Stream get quickEdit => _quickEdit.stream;

  var sortedFields = List<String>();
  var columnSelection = MultiSelectionModel<String>();
  var rows = List<List<String>>();
  var ids = List<String>();
  List<int> quickEditFieldIndices;

  List<int> pageLimits = [5, 10, 25, 50, 100];
  bool canShowTable = false;

  List<String> get fields => sortedFields;
  List<String> get selectedColumns => columnSelection.selectedValues.toList();
  List<Tuple2<int, int>> get selectedCell => selectionModel.selectedValues
      .map((id) => Tuple2(ids.indexOf(id), -1))
      .toList();

  String get currentPageLimit => pageLimit.toString();
  int get startingIndex => page * pageLimit;

  get itemRenderer => (id) => id.toString();

  // Humanized index only during printing. Additional "with" to make pagination looks continual.
  String get paginationInfo => filteredData != null
      ? 'Showing items (${startingIndex + 1}-${startingIndex + filteredData.length}) with '
      : '';

  void ngOnInit() {
    columnSelection.selectionChanges.listen(_handleColumnChanges);
  }

  void ngOnChanges(Map<String, SimpleChange> changes) async {
    if (changes.containsKey('filteredData')) _handleFilteredDataChange();
  }

  void tableSelectionChange(List<Tuple2<int, int>> selections) {
    final cell = selections.isNotEmpty ? selections.first : null;
    final id = cell != null ? ids[cell.item1] : null;
    _selection.add([id]);
  }

  void tableCellClicked(Tuple3<int, int, bool> clickedCell) {
    final id = ids[clickedCell.item1];
    final wasSelected = clickedCell.item3;
    assert(id != null,
        'id of row ${clickedCell.item1} was not found when selecting row in table');

    if (wasSelected) {
      selectionModel.deselect(id);
    } else {
      selectionModel.select(id);
    }

    _selection.add(selectionModel.selectedValues.toList());
  }

  void tableQuickEdit(Tuple3<int, int, String> edition) {
    final id = ids[edition.item1];
    final field = sortedFields[edition.item2];
    _quickEdit.add(Tuple3(id, field, edition.item3));
  }

  void clearSelection() {
    selectionModel.clear();
    _selection.add(selectionModel.selectedValues.toList());
  }

  void filterChange(changes) {
    _filterChange.add(changes);
  }

  void gotoNextPage() {
    _nextPageTrigger.add(null);
  }

  void gotoPreviousPage() {
    _previousPageTrigger.add(null);
  }

  void pageLimitChange(change) {
    pageLimit = change;
    _pageLimitChange.add(change);
  }

  void _handleFilteredDataChange() {
    // Record all the unique fields
    var fieldSet = Set<String>();
    for (var data in filteredData) {
      fieldSet.addAll(data.props.keys.toSet());
    }
    sortedFields = fieldSet.toList();

    if (schema != null) {
      // Default selected field base on schema's priority
      columnSelection.selectAll(sortedFields.where((field) {

        final rule = schema.rules.firstWhere((rule) => rule.name == field,
          orElse: () => null);

        return rule != null && (rule.isMandatory || rule.isFilter);
      }));
    } else {
      // No schema? No sort and select all fields
      columnSelection.selectAll(sortedFields);
    }

    // Figure out the column for quick edit base on the sorted fields.
    if (quickEditFields != null) {
      quickEditFieldIndices = quickEditFields
          .map((field) => sortedFields.indexOf(field))
          .where((index) => index != -1)
          .toList();
    }

    _repopulateTable();
    canShowTable = true;
  }

  void _handleColumnChanges(List<SelectionChangeRecord<dynamic>> changes) {
    _repopulateTable();
  }

  void _repopulateTable() {
    ids.clear();
    rows.clear();

    for (var data in filteredData) {
      ids.add(data.id);
      rows.add(selectedColumns.map((field) {
        var value = data.props[field]?.value?.toString() ?? '';

        //TODO: hack to do prevent break up
        // if (schema != null
        //     && (schema.id == MetaDataService.maintainableItemSchemaFixedName || schema.id == MetaDataService.generatedTaskSchemaFixedName)
        //     && field == schema.extra[MetaDataService.assetIdPropsKey]) {
        //   // prevent css to break our long-floc-id.
        //   value = value.replaceAll(RegExp(r'-'), '‑');
        // }

        return value;
      }).toList());
    }
  }
}
