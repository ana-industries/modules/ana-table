import 'dart:async' show StreamController;

import 'package:angular/angular.dart';
import 'package:angular_components/focus/focus.dart';
import 'package:angular_components/material_button/material_button.dart';
import 'package:angular_components/material_icon/material_icon.dart';
import 'package:angular_components/material_input/material_input.dart';
import 'package:angular_components/material_popup/material_popup.dart';
import 'package:angular_components/src/laminate/popup/popup_source.dart';
import 'package:tuple/tuple.dart' show Tuple2, Tuple3;

/// AnaTableComponent is simple HTML table with ANA magic.
///
/// `fields` is the header of each column. TODO: change the name to correctly say it HEADER!
/// `rows` are a 2D array contain a list of string corresponding to the header for each row,
///
/// Selection is supported in 3 `pickMode`. Row, Column and Cell.
/// It can be `multi` selection or single.
/// If the user of this component want to handle click, can do, just sign up for `click` event.
///
/// If `explicitSelection` is true, `explicitSelectedCell` will be highlighed and all selection handling in
/// this component will be disable.
///
/// `quickEditFieldIndices` mark which column in this table should support a quick inline edit.
/// Passing in the column index will do. A small button and a popup will show when click.
/// Then any new input will be fire out with `quickEdit`.
///
@Component(
  selector: 'ana-table',
  directives: [
    AutoFocusDirective,
    MaterialButtonComponent,
    MaterialIconComponent,
    materialInputDirectives,
    MaterialPopupComponent,
    PopupSourceDirective,
    NgClass,
    NgFor,
    NgIf
  ],
  styleUrls: ['ana_table_component.css', 'style/bootstrap-4-table.css'],
  templateUrl: 'ana_table_component.html',
)
class AnaTableComponent implements OnChanges {
  static const String pickModeRow = "row";
  static const String pickModeColumn = "column";
  static const String pickModeCell = "cell";

  @Input()
  List<String> fields;
  @Input()
  List<List<String>> rows;
  @Input()
  String pickMode = pickModeRow;
  @Input()
  bool lineNumber = false;
  @Input()
  bool wrapText = false;
  @Input()
  int startingIndex = 0;
  @Input()
  bool multi = false;
  @Input()
  bool explicitSelection = false;
  @Input()
  List<Tuple2<int, int>> explicitSelectedCell;
  @Input()
  List<int> quickEditFieldIndices;

  final _selection = StreamController<List<Tuple2<int, int>>>(); // row, col
  final _click =
      StreamController<Tuple3<int, int, bool>>(); // row, col, was selected
  final _quickEdit =
      StreamController<Tuple3<int, int, String>>(); // row, col, edited value

  @Output()
  Stream<List<Tuple2<int, int>>> get selectionChange => _selection.stream;
  @Output()
  Stream<Tuple3<int, int, bool>> get click => _click.stream;
  @Output()
  Stream<Tuple3<int, int, String>> get quickEdit => _quickEdit.stream;

  @ViewChildren(PopupSourceDirective)
  List<PopupSourceDirective> popupSources;

  var _selectedCell = List<Tuple2<int, int>>();
  int overRow = -1;
  int overColumn = -1;

  Tuple2<int, int> quickEditCell;
  PopupSource quickEditPopupSource;
  String quickEditValue = '';
  bool quickEditPopupVisible = false;

  @override
  void ngOnChanges(Map<String, SimpleChange> changes) {
    if (changes.containsKey('explicitSelectedCell')) {
      _selectedCell = explicitSelectedCell.toList();
    }
  }

  Map<String, bool> getStyle() {
    if (wrapText) {
      return {'wrap': true};
    } else {
      return {'no-wrap': true};
    }
  }

  void clickCell(int i, int j, dynamic event) {
    if (explicitSelection) {
      _click.add(Tuple3(i, j, isSelected(i, j)));
    } else {
      if (multi && event.ctrlKey) {
        _selectedCell.add(Tuple2(i, j));
      } else {
        var cell = Tuple2(i, j);
        var previous =
            _selectedCell.isNotEmpty ? _selectedCell.removeLast() : null;
        if (previous != cell) {
          _selectedCell.add(cell);
        }
      }

      _selection.add(_selectedCell);
    }
  }

  void overCell(int i, int j) {
    overRow = i;
    overColumn = j;
  }

  void outCell(int i, int j) {
    if (overRow == i) {
      overRow = -1;
    }
    if (overColumn == j) {
      overColumn = -1;
    }
  }

  bool hoveringRow(int i) => pickMode == pickModeRow && overRow == i;
  bool hoveringColumn(int j) => pickMode == pickModeColumn && overColumn == j;
  bool hoveringCell(int i, int j) =>
      pickMode == pickModeCell && overRow == i && overColumn == j;

  bool isSelected(int i, int j) {
    switch (pickMode) {
      case pickModeRow:
        return selectedRow(i);
        break;
      case pickModeColumn:
        return selectedColumn(j);
        break;
      case pickModeCell:
        return selectedCell(i, j);
        break;
      default:
        return false;
    }
  }

  bool selectedRow(int i) =>
      pickMode == pickModeRow &&
      _selectedCell.indexWhere((t) => t.item1 == i) > -1;
  bool selectedColumn(int j) =>
      pickMode == pickModeColumn &&
      _selectedCell.indexWhere((t) => t.item2 == j) > -1;
  bool selectedCell(int i, int j) =>
      pickMode == pickModeCell &&
      _selectedCell.indexWhere((t) => t.item1 == i && t.item2 == j) > -1;

  bool enabledQuickEdit(int j) {
    if (quickEditFieldIndices == null || quickEditFieldIndices.isEmpty) {
      return false;
    }
    return quickEditFieldIndices.contains(j);
  }

  void startQuickEdit(int i, int j, dynamic event) {
    // Prevent click through table cell
    event.stopPropagation();

    quickEditCell = Tuple2(i, j);
    quickEditPopupVisible = true;
    quickEditValue = rows[i][j];
    quickEditPopupSource = popupSources[i];
  }

  void saveQuickEdit(event) {
    // Prevent "enter" in the material input
    event.preventDefault();

    quickEditPopupVisible = false;

    _quickEdit
        .add(Tuple3(quickEditCell.item1, quickEditCell.item2, quickEditValue));
  }

  String rowIndex(index) => (startingIndex + index + 1).toString();
}
