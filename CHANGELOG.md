## 0.0.3

- Fixed an exception caused by table assumes a rule must be presented, but schema can change and data might not migrate correctly.

## 0.0.2

- Adapt ana-metadata changes.

## 0.0.1

- Initial version, created by Stagehand
