# Table for ANA

## Features

### ANATableComponent

Is a normal HTML table with standardized look, functionality for selections and quick edit. It does not manipulate the input data, It just display what is passed in.

### ANADataTableComponent

A more sophisticated table on top of ANATableComponent. It added functionality to do pagination, filtering and column selection. Only data derived from ana_metadata can use in this component.

## Development

### Git branches

`master` is protected and reserve for main application development.

All active development should happen on `dev` branch. You should always branch out from `dev` and create push request against the `dev`.

### Pub Packages and ANA Modules

We open sourced a few building blocks of ANA, and these projects are ANA Modules. The cost of this is a tedious packages management in the main application.

Developers went through the trial projects will not be foreign to ANA Modules. Just a rule of thumb, we should always use the git packages unless you are working on feature related to that module.

### Style guide

[Dart's official guide](https://dart.dev/guides/language/effective-dart)

### Code Formatting

For `.dart`, use `dartfmt`. It is a tool came with Dart SDK. Do this in your command line `dartfmt -w .`

For `.html` `.scss` `.yaml`, use [prettier](https://prettier.io/docs/en/index.html). Install it and type `prettier --write "**/*.html" "**/*.scss" "**/*.yaml"` in your command line.

Better, if your IDE support it, just install the extension of these and enabled 'format on save'. It will be easier.

### Rules

- Commit and push your branch to GitLab, often.
- Have fun.

## TODO

- Examples
- FilterComponent need a lot of work, especially the range slider.
- A sorting column. (Just an event, not the actually sorting)
