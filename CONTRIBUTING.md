Please be mindful that, this project is an integral module with the ANA's main 
application. It is not yet ready to be mass-adoption or easily use by other 
project without a considerate amount of adaption on both side.

All contribution, in the form of reporting issue, creating pull request, 
testing in general are very much welcome. But the maintainers have the final say
for any changes that make into the master branch. There are only one critirea:

1. The changes must not break ANA main application. 

Usually only changes that directly fix a bug or feature request originated from
ANA main application will be consider. 